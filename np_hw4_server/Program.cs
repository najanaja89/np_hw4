﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace np_hw4_server
{
    class Program
    {
        static void Main(string[] args)
        {
            Socket srvSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            srvSocket.Bind(new IPEndPoint(IPAddress.Any, 12345));

            //var savePath = Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.MyPictures)).FullName+"\\Screenshot"+DateTime.Now.ToString()+".bmp";
        
            EndPoint clientEndPoint = new IPEndPoint(0, 0);
            byte[] bufUdp = new byte[63*1024];
            while (true)
            {
                int recSize = srvSocket.ReceiveFrom(bufUdp, ref clientEndPoint);
                if (Encoding.UTF8.GetString(bufUdp, 0, recSize) == "GetScreenShot")
                {               
                    var bmp = GetScreenShot();
                    //byte[] bufImg = ImageToByteArray(bmp);
                    //bmp.Save("screenShot.bmp");

                    //Thread.Sleep(20);
                    //srvSocket.Send(Encoding.UTF8.GetBytes("test"), SocketFlags.None);
                    Console.WriteLine(Encoding.UTF8.GetString(bufUdp, 0, recSize));
                }
            }


            Console.ReadLine();
        }

        private static byte[] ImageToByteArray(System.Drawing.Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, imageIn.RawFormat);
                return ms.ToArray();
            }
        }


        private static Bitmap GetScreenShot()
        {
            Graphics graph = null;
            var bmp = new Bitmap(Screen.PrimaryScreen.Bounds.Width, Screen.PrimaryScreen.Bounds.Height);
            graph = Graphics.FromImage(bmp);
            graph.CopyFromScreen(0, 0, 0, 0, bmp.Size);
            return bmp;
        }
    }
}
