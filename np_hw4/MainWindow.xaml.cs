﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace np_hw4
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public Socket client;
        public IPEndPoint endPoint;
        public MainWindow()
        {
            InitializeComponent();
        }

        private void ButtonGetScreenShotClick(object sender, RoutedEventArgs e)
        {
            client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            string str = "GetScreenShot";
            string ipSrv = "127.0.0.1";
            int port = 12345; 
            endPoint = new IPEndPoint(IPAddress.Parse(ipSrv),port);
            int sendSize = client.SendTo(Encoding.UTF8.GetBytes(str), endPoint);
            ThreadPool.QueueUserWorkItem(ClientThreadRoutine);
        }

        private void ClientThreadRoutine(object state)
        {
            while (true)
            {
                byte[] buf = new byte[63 * 1024];
                IPEndPoint endPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 12345);
                var data = client.Receive(buf, SocketFlags.None);
            }
        }
    }
}
